package com.android.okalman.car_expenses.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by okalman on 19.9.17.
 */

@DatabaseTable(tableName = "car")
public class Car {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField
    String vin;
    @DatabaseField(canBeNull = false)
    String name;
    @DatabaseField
    String registrationPlate;
    @DatabaseField
    String b64Image;
    @DatabaseField(canBeNull = false)
    Integer kilometers;
    @DatabaseField(canBeNull = false)
    Integer fuelTankCapacity;


}
