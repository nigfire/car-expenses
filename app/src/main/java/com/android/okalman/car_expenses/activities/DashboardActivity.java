package com.android.okalman.car_expenses.activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.android.okalman.car_expenses.R;
import com.android.okalman.car_expenses.interfaces.IcallBack;
import com.android.okalman.car_expenses.listAdapters.DrawerAdapter;

import java.util.ArrayList;


public class DashboardActivity extends AppCompatActivity  implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Toolbar tb;
    private DrawerLayout navDrawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ListViewCompat navDrawerContentList;
    private FloatingActionButton addCarButton;
    private ListViewCompat list;
    private IcallBack<String> callbackFromDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        callbackFromDrawer = new IcallBack<String>() {
            @Override
            public void callBack(String param) {
                if(navDrawerLayout !=null){
                    navDrawerLayout.closeDrawers();
                }
            }
        };
        initializeViews();
        tb.setTitle("MyCar");
        this.setSupportActionBar(tb);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }
    private void initializeViews(){
        tb = (Toolbar) this.findViewById(R.id.my_toolbar);
        navDrawerLayout = (DrawerLayout) findViewById(R.id.dashboard_drawer);
        navDrawerContentList = (ListViewCompat) findViewById(R.id.dashboard_drawer_content_list);
        ArrayList<String> cars= new ArrayList();
        cars.add("header");
        for(int i=0; i<1000 ; i++){
            cars.add("car"+i);
        }
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,navDrawerLayout,R.string.drawer_open,
                R.string.drawer_close){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle("MyCar");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("MyDrawer");
            }
        };
        navDrawerLayout.addDrawerListener(actionBarDrawerToggle);


        navDrawerContentList.setAdapter(new DrawerAdapter(this,cars,callbackFromDrawer));
        navDrawerContentList.setOnItemClickListener(this);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            Log.d(this.getClass().getName(), "onOptionsItemSelected: drawer button clicked");
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_my_car:
                // User chose the "Settings" item, show the app settings UI...
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
