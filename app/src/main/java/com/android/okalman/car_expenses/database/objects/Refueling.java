package com.android.okalman.car_expenses.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.types.BooleanCharType;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by okalman on 20.9.17.
 */

@DatabaseTable(tableName = "refueling")
public class Refueling {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField(canBeNull = false)
    BooleanCharType toFull;
    @DatabaseField(canBeNull = false)
    Float liters;
    @DatabaseField(canBeNull = false)
    Float price;
    @DatabaseField(canBeNull = false)
    Byte fuelType;
    @DatabaseField(canBeNull = false)
    Date date;
    @DatabaseField(foreign = true)
    Car car;
    @DatabaseField
    String note;
    @DatabaseField(foreign = true)
    GasStation gasStation;

}
