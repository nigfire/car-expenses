package com.android.okalman.car_expenses.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by okalman on 20.9.17.
 */

@DatabaseTable(tableName = "gasstation")
public class GasStation {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField(canBeNull = false)
    String name;
    @DatabaseField(canBeNull = false)
    Float latitude;
    @DatabaseField(canBeNull = false)
    Float longitude;

    String description;
}
