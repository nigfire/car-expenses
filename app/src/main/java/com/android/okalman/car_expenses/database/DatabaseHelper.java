package com.android.okalman.car_expenses.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.okalman.car_expenses.R;
import com.android.okalman.car_expenses.database.objects.Car;
import com.android.okalman.car_expenses.database.objects.GasStation;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by okalman on 17.9.17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "carexpenses.db";
    private static final int DATABASE_VERSION = 1;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Car.class);
            TableUtils.createTable(connectionSource, GasStation.class);
            TableUtils.createTable(connectionSource, Car.class);

        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource,Car.class, false);
            TableUtils.dropTable(connectionSource, GasStation.class, false);
            TableUtils.dropTable(connectionSource, Car.class, false);

            onCreate(database, connectionSource);
        }catch (SQLException e){

        }
    }
}
