package com.android.okalman.car_expenses.listAdapters;


import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.okalman.car_expenses.R;
import com.android.okalman.car_expenses.activities.NewCarActivity;
import com.android.okalman.car_expenses.interfaces.IcallBack;

import java.util.List;

/**
 * Created by okalman on 9/23/17.
 */

public class DrawerAdapter extends BaseAdapter implements View.OnClickListener{

    private List<String>data;
    private AppCompatActivity context;
    protected IcallBack<String> callback;

    public DrawerAdapter(AppCompatActivity context, List<String> data, IcallBack<String> callback){
        this.context = context;
        this.data = data;
        this.callback = callback;
    }

    @Override
    public int getCount() {
       return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
       return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(getItemViewType(i)==0){
            return inflateHeader(view, viewGroup);
        }
        else{
            return inflateContentOfList(i, view, viewGroup);
        }

    }
    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return 0;
        }else{
            return 1;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    View inflateHeader(View view, ViewGroup viewGroup){
        View header = view ;
        if(header == null || !(header.getTag() instanceof HeadHolder)){
            Log.i(this.getClass().getName(), "inflateHeader: creating header");
            LayoutInflater inflater = context.getLayoutInflater();
            header = inflater.inflate(R.layout.layout_dashboard_drawer_header, null);
            HeadHolder holder = new HeadHolder();
            holder.annotationKm = (TextView) header.findViewById(R.id.drawer_header_text_km_annotation);
            holder.annotationVin = (TextView) header.findViewById(R.id.drawer_header_text_vin_annotation);
            holder.carImage = (ImageView) header.findViewById(R.id.drawer_header_image_car);
            holder.carName = (TextView) header.findViewById(R.id.drawer_header_text_car_name);
            holder.km = (TextView) header.findViewById(R.id.drawer_header_text_km);
            holder.vin  = (TextView) header.findViewById(R.id.drawer_header_text_vin);
            holder.addCar = (FloatingActionButton) header.findViewById(R.id.buttonAddCar);
            holder.addCar.setTag("ADD_CAR");
            holder.addCar.setOnClickListener(this);
            header.setTag(holder);
        }else{
            Log.i(this.getClass().getName(), "inflateHeader: reusing header");
        }
        HeadHolder headHolder = (HeadHolder) header.getTag();
        /**
         * here we should modify content of holder views
         */

        return header;
    }

    View inflateContentOfList(int i, View view, ViewGroup viewGroup){
        View item = view ;
        if(item == null || !(item.getTag() instanceof  ItemHolder)){
            LayoutInflater inflater = context.getLayoutInflater();
            item = inflater.inflate(R.layout.layout_dashboard_drawer_item, null);
            ItemHolder holder= new ItemHolder();
            holder.car = (TextView) item.findViewById(R.id.drawer_item_car);
            item.setTag(holder);
        }
        ItemHolder holder = (ItemHolder) item.getTag();
        holder.car.setText(data.get(i));
        return item;
    }

    @Override
    public void onClick(View v) {
        if(v.getTag() !=null && v.getTag().equals("ADD_CAR")){
            Intent intent = new Intent(context,NewCarActivity.class);
            callback.callBack("hideDrawer");
            context.startActivity(intent);
        }
    }


    private static class HeadHolder{

        TextView carName, annotationKm, annotationVin, km, vin;
        ImageView carImage;
        FloatingActionButton addCar;

    }

    private static class ItemHolder{
        TextView car;
    }

    private static class UniversalHolder{
        HeadHolder head;
        ItemHolder item;

    }
}
