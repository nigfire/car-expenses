package com.android.okalman.car_expenses.interfaces;

/**
 * Created by okalman on 9/24/17.
 */

public interface IcallBack<T> {
    void callBack(T param);
}
